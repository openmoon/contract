// SPDX-License-Identifier: AGPL-3.0-or-later

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "./LiquifiedStaking.sol";
import "./Liquified.sol";
import "./MathUtils.sol";
import "./Governable.sol";

pragma solidity >=0.8.0 <=0.8.4;

contract LiquifiedStaking is ReentrancyGuard, Governable {
    using SafeERC20 for IERC20;
    //ln2 * 2^127 / t(1/2)
    uint256 public lambdaConst = 4.5498796918501792850683669514690 * 10**31;
    address internal LQFD;
    mapping(address => PoolData) public pools;
    mapping(address => uint256) public accountRStake;

    struct PoolData {
        MathUtils.ufixed512 rewardPerR;
        uint256 lastUpdateTime;
        uint256 availableRewardAmount;
        uint256 rewardReserve;
        uint256 rValueStakedTotal;
        mapping(address => uint256) rValueStaked;
        mapping(address => MathUtils.ufixed512) startingRPR;
    }

    constructor() {
        governor = tx.origin;
    }

    function initialize() external {
        require(LQFD == address(0), "LQFD: Already initialized");
        LQFD = msg.sender;
    }

    function setLambda(uint256 lambda) public onlyGovernance {
        lambdaConst = lambda;
    }

    function transferTokens(
        address _token,
        address to,
        uint256 amount
    ) external nonReentrant {
        require(msg.sender == LQFD, "LQFD: Access forebidden.");
        IERC20(_token).safeTransfer(to, amount);
    }

    function updatePool(address reward) public {
        require(reward != LQFD, "illegal reward");
        PoolData storage pool = pools[reward];
        uint256 deltaTime = block.timestamp - pool.lastUpdateTime;
        uint256 rewardBal = IERC20(reward).balanceOf(address(this));
        uint256 rewardAdded = rewardBal - pool.rewardReserve;
        if (pool.rValueStakedTotal > 0) {
            uint256 newAvailableReward = computeAvailableReward(
                pool.availableRewardAmount,
                deltaTime
            );
            pool.rewardPerR = MathUtils.add_512_512_512(
                pool.rewardPerR,
                MathUtils.div_256_256_512(
                    (pool.availableRewardAmount - newAvailableReward),
                    pool.rValueStakedTotal
                )
            );
            pool.availableRewardAmount = newAvailableReward;
        }
        pool.availableRewardAmount += rewardAdded;
        pool.rewardReserve = rewardBal;
        pool.lastUpdateTime = block.timestamp;
    }

    function updatePoolAccount(address reward, address account) internal {
        PoolData storage pool = pools[reward];
        updatePool(reward);
        uint256 rewardAmount = MathUtils.mul_256_512_256(
            pool.rValueStaked[account],
            (
                MathUtils.sub_512_512_512(
                    pool.rewardPerR,
                    pool.startingRPR[account]
                )
            )
        );
        pool.rewardReserve -= rewardAmount;
        IERC20(reward).safeTransfer(account, rewardAmount);
        pool.startingRPR[account] = pool.rewardPerR;
    }

    function deposit(address reward, uint256 amount) external nonReentrant {
        updatePoolAccount(reward, msg.sender);
        PoolData storage pool = pools[reward];
        IERC20(LQFD).safeTransferFrom(msg.sender, address(this), amount);
        uint256 rValue = LiquifiedToken(LQFD).rFromToken(amount);
        pool.rValueStaked[msg.sender] += rValue;
        accountRStake[msg.sender] += rValue;
        pool.rValueStakedTotal += rValue;
    }

    function withdraw(address reward, uint256 amount) external nonReentrant {
        updatePoolAccount(reward, msg.sender);
        PoolData storage pool = pools[reward];
        IERC20(LQFD).safeTransfer(msg.sender, amount);
        uint256 rValue = LiquifiedToken(LQFD).rFromToken(amount);
        pool.rValueStaked[msg.sender] -= rValue;
        accountRStake[msg.sender] -= rValue;
        pool.rValueStakedTotal -= rValue;
    }

    function accountStake(address reward, address account)
        external
        view
        returns (uint256)
    {
        return
            LiquifiedToken(LQFD).tokenFromR(
                pools[reward].rValueStaked[account]
            );
    }

    function computeAvailableReward(
        uint256 currentAvailReward,
        uint256 deltaTime
    ) internal view returns (uint256 availableRewardAmount) {
        uint256 divisor = MathUtils.generalExp(deltaTime * lambdaConst, 127);
        uint256 precision = 2**127;
        while (divisor >= 2**128) {
            divisor /= 2;
            precision /= 2;
        }
        MathUtils.ufixed512 memory multiplier = MathUtils.div_256_256_512(
            precision,
            divisor
        );
        return MathUtils.mul_256_512_256(currentAvailReward, multiplier);
    }

    function poolUserInfo(address reward, address account)
        external
        view
        returns (
            string memory rewardName,
            string memory rewardSymbol,
            uint256 stakeAccount,
            uint256 stakeTotal,
            uint256 rewardAvailable,
            uint256 rewardPending,
            uint256 LQFDBalanceAccount,
            uint256 LQFDAllowance
        )
    {
        PoolData storage pool = pools[reward];
        rewardName = IERC20Metadata(reward).name();
        rewardSymbol = IERC20Metadata(reward).symbol();
        stakeAccount = LiquifiedToken(LQFD).tokenFromR(
            pool.rValueStaked[account]
        );
        stakeTotal = LiquifiedToken(LQFD).tokenFromR(pool.rValueStakedTotal);
        rewardAvailable = pool.availableRewardAmount;
        LQFDBalanceAccount = LiquifiedToken(LQFD).balanceOf(account);
        LQFDAllowance = LiquifiedToken(LQFD).allowance(account, address(this));

        if (pool.rValueStakedTotal > 0) {
            MathUtils.ufixed512 memory newRewardPerR = MathUtils
                .add_512_512_512(
                    pool.rewardPerR,
                    MathUtils.div_256_256_512(
                        (pool.availableRewardAmount -
                            computeAvailableReward(
                                pool.availableRewardAmount,
                                block.timestamp - pool.lastUpdateTime
                            )),
                        pool.rValueStakedTotal
                    )
                );
            MathUtils.ufixed512 memory adjustedRpR = MathUtils.sub_512_512_512(
                newRewardPerR,
                pool.startingRPR[account]
            );

            rewardPending = MathUtils.mul_256_512_256(
                pool.rValueStaked[account],
                adjustedRpR
            );
        } else {
            rewardPending = 0;
        }
    }
}
