// SPDX-License-Identifier: AGPL-3.0-or-later

import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "./MathUtils.sol";
import "./Staking.sol";

pragma solidity >=0.8.0 <=0.8.4;

contract OpenMoon is IERC20, ReentrancyGuard {
    using Address for address;

    struct LPRecord {
        address token0;
        address token1;
    }

    event RegisterLP(address indexed pair);
    event UnregisterLP(address indexed pair, string reason);

    mapping(address => uint256) private value;
    mapping(address => mapping(address => uint256)) private _allowances;
    mapping(address => LPRecord) private records;
    uint256 public tokensTotal = 0; //total amount of tokens of redistribution-ineligible accounts
    uint256 public rTokensTotal = 2_000_000e18; //total amount of tokens of redistribution-eligible accounts
    uint256 public rValueTotal = rTokensTotal; //total amount of R-values of redistribution-eligible accounts

    string public constant symbol = "OPMN";
    string public constant name = "OpenMoon";
    uint8 public constant decimals = 18;
    uint8 private constant percentBurned = 2;
    uint8 private constant percentRedistributed = 2;
    uint8 private constant percentLiquidified = 3;

    bool public activated = false;
    address public stakingContract;
    address public presaleContract;

    constructor() {
        stakingContract = address(new OpenMoonStakingContract());
        presaleContract = msg.sender;
        value[msg.sender] = rTokensTotal;
        emit Transfer(address(0), msg.sender, rTokensTotal);
    }

    function activate() external nonReentrant {
        require(msg.sender == address(presaleContract), "OPMN: Unauthorized");
        activated = true;
    }

    function totalSupply() public view override returns (uint256) {
        return tokensTotal + rTokensTotal;
    }

    function allowance(address owner, address spender)
        public
        view
        override
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount)
        public
        override
        nonReentrant
        returns (bool)
    {
        _approve(msg.sender, spender, amount);
        return true;
    }

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public override nonReentrant returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][msg.sender];
        require(
            currentAllowance >= amount,
            "OPMN: transfer amount exceeds allowance"
        );
        _approve(sender, msg.sender, currentAllowance - amount);

        return true;
    }

    function transfer(address recipient, uint256 amount)
        public
        override
        nonReentrant
        returns (bool)
    {
        _transfer(msg.sender, recipient, amount);
        return true;
    }

    function burn(uint256 amount) public nonReentrant returns (bool) {
        require(balanceOf(msg.sender) >= amount);
        subtractBalance(msg.sender, amount);
        emit Transfer(msg.sender, address(0), amount);
        return true;
    }

    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal {
        require(owner != address(0), "OPMN: approve from the zero address");
        require(spender != address(0), "OPMN: approve to the zero address");
        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function balanceOf(address account) public view override returns (uint256) {
        if (isExcluded(account)) return value[account];
        return tokenFromR(value[account]);
    }

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal {
        require(sender != address(0), "OPMN: transfer from the zero address");
        require(recipient != address(0), "OPMN: transfer to the zero address");
        require(
            balanceOf(sender) >= amount,
            "OPMN: transfer amount exceeds balance"
        );
        if (!activated) {
            require(
                sender == presaleContract,
                "OPMN: transfers unavailable yet"
            );
        }
        subtractBalance(sender, amount);
        bool takeFee = activated &&
            (sender != stakingContract) &&
            (recipient != stakingContract);
        if (takeFee) {
            uint256 amountBurned = (amount * percentBurned) / 100;
            uint256 amountRedistributed = (amount * percentRedistributed) / 100;
            distributeValue(amountRedistributed);
            amount = amount - amountRedistributed - amountBurned;

            if (isLP(recipient)) {
                //try to liquidify, unregister LP on error
                uint256 amountLiquidified = (amount * percentLiquidified) / 100;
                try this.provideLiquidity(recipient, amountLiquidified) {
                    amount = amount - amountLiquidified;
                } catch Error(string memory reason) {
                    unregisterLP(recipient);
                    emit UnregisterLP(recipient, reason);
                }
            }
        }
        addBalance(recipient, amount);
        emit Transfer(sender, recipient, amount);
    }

    //Checks if account is not eligible for redistribution.
    function isExcluded(address account) public view returns (bool) {
        return isLP(account);
    }

    function rFromToken(uint256 amount) public view returns (uint256) {
        return (amount * rValueTotal) / rTokensTotal;
    }

    function tokenFromR(uint256 amount) public view returns (uint256) {
        return (amount * rTokensTotal) / rValueTotal;
    }

    function distributeValue(uint256 amount) internal {
        rTokensTotal = rTokensTotal + amount;
    }

    function addBalance(address account, uint256 amount) internal {
        if (isExcluded(account)) {
            tokensTotal += amount;
            value[account] += amount;
        } else {
            uint256 rValue = rFromToken(amount);
            rTokensTotal += amount;
            rValueTotal += rValue;
            value[account] += rValue;
        }
    }

    function subtractBalance(address account, uint256 amount) internal {
        if (isExcluded(account)) {
            tokensTotal -= amount;
            value[account] -= amount;
        } else {
            uint256 rValue = rFromToken(amount);
            rTokensTotal -= amount;
            rValueTotal -= rValue;
            value[account] -= rValue;
        }
    }

    function convertToExcluded(address account) internal {
        require(!isExcluded(account), "OPMN: Account already excluded.");
        uint256 rValue = value[account];
        uint256 tokenV = tokenFromR(rValue);
        rValueTotal -= rValue;
        rTokensTotal -= tokenV;

        tokensTotal += tokenV;
        value[account] = tokenV;
    }

    function convertFromExcluded(address account) internal {
        require(isExcluded(account), "OPMN: Account not excluded.");
        uint256 tokenV = value[account];
        uint256 rValue = rFromToken(tokenV);
        tokensTotal -= tokenV;

        rTokensTotal += tokenV;
        rValueTotal += rValue;
        value[account] = rValue;
    }

    function isLP(address account) internal view returns (bool) {
        LPRecord storage rec = records[account];
        return (rec.token0 != address(0) && rec.token1 != address(0));
    }

    function unregisterLP(address account) internal {
        convertFromExcluded(account);
        LPRecord storage rec = records[account];
        rec.token0 = address(0);
        rec.token1 = address(0);
    }

    function provideLiquidity(address LP, uint256 amount)
        external
        nonReentrant
    {
        require(msg.sender == address(this), "OPMN: Access forebidden.");
        LPRecord storage rec = records[LP];
        bool OPMNtoken0 = rec.token0 == address(this);
        (uint256 reserve0, uint256 reserve1, ) = IUniswapV2Pair(LP)
            .getReserves();
        IERC20 otherToken = IERC20(OPMNtoken0 ? rec.token1 : rec.token0);
        uint256 OPMNReserve = OPMNtoken0 ? reserve0 : reserve1;
        uint256 otherReserve = OPMNtoken0 ? reserve1 : reserve0;
        uint256 amountIn = MathUtils.sqrt(
            OPMNReserve * (OPMNReserve + amount)
        ) - OPMNReserve;
        amountIn = (amountIn * 1025) / 1000;
        uint256 amountOut = (otherReserve * amountIn) /
            (OPMNReserve + amountIn);
        amountOut = (amountOut * 975) / 1000;
        uint256 otherBalBefore = otherToken.balanceOf(stakingContract);
        addBalance(LP, amountIn);
        IUniswapV2Pair(LP).swap(
            OPMNtoken0 ? 0 : amountOut,
            OPMNtoken0 ? amountOut : 0,
            stakingContract,
            new bytes(0)
        );
        uint256 otherReceived = otherToken.balanceOf(stakingContract) -
            otherBalBefore;
        OpenMoonStakingContract(stakingContract).transferTokens(
            address(otherToken),
            LP,
            otherReceived
        );
        addBalance(LP, amount - amountIn);
        IUniswapV2Pair(LP).mint(stakingContract);
    }

    function registerLP(address pair) external nonReentrant {
        convertToExcluded(pair);
        IUniswapV2Pair LP = IUniswapV2Pair(pair);
        address token0 = LP.token0();
        address token1 = LP.token1();
        require(
            (token0 == address(this)) != (token1 == address(this)),
            "OPMN: Invalid LP"
        );
        require(
            token0 != address(0) && token1 != address(0),
            "OPMN: Invalid LP!"
        );
        LPRecord memory rec = LPRecord(token0, token1);
        records[pair] = rec;
        emit RegisterLP(pair);
    }
}
