// SPDX-License-Identifier: AGPL-3.0-or-later

import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol";
import "./IWETH.sol";
import "./OpenMoonToken.sol";

pragma solidity >=0.8.0 <=0.8.4;

contract OpenMoonPreSale is ReentrancyGuard {
    using Address for address;
    using SafeERC20 for IERC20;

    address public OPMN;
    address public constant WAVAX = 0xB31f66AA3C1e785363F0875A1B74E27b85FD66c7;
    IUniswapV2Factory public constant cakeFactory =
        IUniswapV2Factory(0xefa94DE7a4656D787667C749f7E1223D71E9FD88);
    IUniswapV2Factory public constant joeFactory =
        IUniswapV2Factory(0x9Ad6C38BE94206cA50bb0d90783181662f0Cfa10);

    uint256 public constant OPMNPerAvax = 100;
    uint256 public constant hardCapAvax = 5000 ether;
    uint256 public endTime;
    bool public active = true;

    constructor(uint256 _endTime) {
        endTime = _endTime;
        OPMN = address(new OpenMoon());
    }

    receive() external payable nonReentrant {
        require(msg.sender == tx.origin);
        require(block.timestamp < endTime, "Presale time is over.");
        require(active, "Presale has ended.");
        require(address(this).balance <= hardCapAvax, "Hard Cap reached.");
        uint256 tokens = msg.value * OPMNPerAvax;
        IERC20(OPMN).safeTransfer(msg.sender, tokens);
    }

    //Finalizes presale ensuring end conditions are met, supplies initial liquidity, sends LP to staking
    function finalize() external nonReentrant {
        require(
            address(this).balance >= hardCapAvax || block.timestamp >= endTime,
            "Conditions not met"
        );
        active = false;

        if (cakeFactory.getPair(OPMN, WAVAX) == address(0)) {
            cakeFactory.createPair(OPMN, WAVAX);
        }
        if (joeFactory.getPair(OPMN, WAVAX) == address(0)) {
            joeFactory.createPair(OPMN, WAVAX);
        }
        address cakeLP = cakeFactory.getPair(OPMN, WAVAX);
        require(cakeLP != address(0), "CakePair not found!");
        address joeLP = joeFactory.getPair(OPMN, WAVAX);
        require(joeLP != address(0), "JoePair not found!");

        uint256 bal = address(this).balance;
        IWETH(WAVAX).deposit{value: bal}();

        IERC20(WAVAX).safeTransfer(cakeLP, bal / 2);
        IERC20(OPMN).safeTransfer(cakeLP, (bal * OPMNPerAvax) / 2);
        IUniswapV2Pair(cakeLP).mint(OpenMoon(OPMN).stakingContract());

        IERC20(WAVAX).safeTransfer(joeLP, bal / 2);
        IERC20(OPMN).safeTransfer(joeLP, (bal * OPMNPerAvax) / 2);
        IUniswapV2Pair(joeLP).mint(OpenMoon(OPMN).stakingContract());

        OpenMoon(OPMN).activate();
        OpenMoon(OPMN).burn(IERC20(OPMN).balanceOf(address(this)));
    }
}
