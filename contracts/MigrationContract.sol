// SPDX-License-Identifier: AGPL-3.0-or-later
pragma solidity >=0.8.0 <=0.8.4;

import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Factory.sol";
import "./IWETH.sol";
import "./Staking.sol";
import "./Liquified.sol";
import "./MathUtils.sol";

contract OpenMoonMigrationContract is ReentrancyGuard {
    using Address for address;
    using SafeERC20 for IERC20;
    address public LQFD;
    address public stakingContract;

    address public constant oldOPMN =
        0xD8b9F9B8dd11B9F6a1e0C7Da6a2690720FDC8Da3;
    address public constant oldStaking =
        0x5E0113123B63b44F0aFdA2fB2919aB9904c74EDA;

    address public constant WAVAX = 0xB31f66AA3C1e785363F0875A1B74E27b85FD66c7;

    address public constant oldPangoLP =
        0xbBD098c7a0335D4237FbDD14b91EFEd163B29E4a;
    address public constant oldJoeLP =
        0xbE67A9673935ad4ecDfDfF397f944198Eb868996;

    address public constant pangoFactory =
        0xefa94DE7a4656D787667C749f7E1223D71E9FD88;
    address public constant joeFactory =
        0x9Ad6C38BE94206cA50bb0d90783181662f0Cfa10;

    address public joeLP;
    address public pangoLP;

    constructor(address _LQFD) {
        LQFD = _LQFD;
        LiquifiedToken(LQFD).initialize();

        IUniswapV2Factory(joeFactory).createPair(LQFD, WAVAX);
        joeLP = IUniswapV2Factory(joeFactory).getPair(LQFD, WAVAX);

        IUniswapV2Factory(pangoFactory).createPair(LQFD, WAVAX);
        pangoLP = IUniswapV2Factory(pangoFactory).getPair(LQFD, WAVAX);

        stakingContract = LiquifiedToken(LQFD).stakingContract();

        IERC20(oldOPMN).approve(oldStaking, type(uint256).max);
    }

    function migrateOPMN(uint256 amount) external nonReentrant {
        IERC20(oldOPMN).safeTransferFrom(msg.sender, address(this), amount);
        LiquifiedToken(LQFD).mint(msg.sender, amount);
    }

    function migratePangoLP(uint256 amount) external nonReentrant {
        convertAvaxLiquidity(
            oldPangoLP,
            pangoLP,
            msg.sender,
            msg.sender,
            amount
        );
    }

    function migrateJoeLP(uint256 amount) external nonReentrant {
        convertAvaxLiquidity(oldJoeLP, joeLP, msg.sender, msg.sender, amount);
    }

    function convertAvaxLiquidity(
        address oldLp,
        address newLP,
        address from,
        address to,
        uint256 amount
    ) internal {
        if (from == address(this)) {
            IERC20(oldLp).safeTransfer(oldLp, amount);
        } else {
            IERC20(oldLp).safeTransferFrom(from, oldLp, amount);
        }
        (uint256 amountAVAX, uint256 amountOPMN) = IUniswapV2Pair(oldLp).burn(
            address(this)
        );
        addLiquidity(newLP, WAVAX, amountAVAX, amountOPMN, to);
    }

    function updateStaking() external nonReentrant {
        uint256 bal = IERC20(oldOPMN).balanceOf(address(this));
        OpenMoonStakingContract(oldStaking).deposit(oldPangoLP, bal / 2);
        OpenMoonStakingContract(oldStaking).deposit(oldJoeLP, bal / 2);
    }

    function updateStakingRewards() external nonReentrant {
        if (IERC20(oldPangoLP).balanceOf(address(this)) > 0) {
            convertAvaxLiquidity(
                oldPangoLP,
                pangoLP,
                address(this),
                stakingContract,
                IERC20(oldPangoLP).balanceOf(address(this))
            );
        }
        if (IERC20(oldJoeLP).balanceOf(address(this)) > 0) {
            convertAvaxLiquidity(
                oldJoeLP,
                joeLP,
                address(this),
                stakingContract,
                IERC20(oldJoeLP).balanceOf(address(this))
            );
        }
    }

    struct addLiquidityLocals {
        //Offload local vars from stack
        uint256 reserveLQFD;
        uint256 reserveToken;
        uint256 targetReserveToken;
        uint256 targetReserveLQFD;
        uint256 swapTargetToken;
        uint256 swapTargetLQFD;
        uint256 tokenSwapped;
        uint256 LQFDSwapped;
        uint256 tokenReceived;
        uint256 LQFDReceived;
    }

    function addLiquidity(
        address LP,
        address token,
        uint256 tokenAmount,
        uint256 LQFDAmount,
        address mintTo
    ) internal {
        LiquifiedToken(LQFD).mint(address(this), LQFDAmount);

        address token0 = IUniswapV2Pair(LP).token0();

        (uint112 reserve0, uint112 reserve1, ) = IUniswapV2Pair(LP)
            .getReserves();
        if (reserve0 == 0 || reserve1 == 0) {
            IERC20(LQFD).safeTransfer(LP, LQFDAmount);
            IERC20(token).safeTransfer(LP, tokenAmount);

            IUniswapV2Pair(LP).mint(mintTo);
            return;
        }

        addLiquidityLocals memory locals;
        locals.reserveLQFD = token0 == LQFD ? reserve0 : reserve1;
        locals.reserveToken = token0 == LQFD ? reserve1 : reserve0;

        uint256 k = uint256(reserve0) * uint256(reserve1);

        locals.targetReserveToken = locals.reserveToken + tokenAmount;
        locals.targetReserveLQFD = locals.reserveLQFD + LQFDAmount;

        locals.swapTargetToken = MathUtils.sqrt(
            (k * locals.targetReserveToken) / locals.targetReserveLQFD
        );
        locals.swapTargetLQFD = MathUtils.sqrt(
            (k * locals.targetReserveLQFD) / locals.targetReserveToken
        );

        //swap token -> lqfd
        if (locals.swapTargetToken > locals.targetReserveToken) {
            locals.tokenSwapped =
                ((locals.swapTargetToken - locals.targetReserveToken) * 1025) /
                1000;
            locals.LQFDReceived =
                ((locals.targetReserveLQFD - locals.swapTargetLQFD) * 975) /
                1000;
            IERC20(token).safeTransfer(LP, locals.tokenSwapped);
        }

        //swap lqfd -> token
        if (locals.swapTargetLQFD > locals.targetReserveLQFD) {
            locals.LQFDSwapped =
                ((locals.swapTargetLQFD - locals.targetReserveLQFD) * 1025) /
                1000;
            locals.tokenReceived =
                ((locals.targetReserveToken - locals.swapTargetToken) * 975) /
                1000;
            LiquifiedToken(LQFD).mint(LP, locals.LQFDSwapped);
        }

        if (locals.tokenSwapped != 0 || locals.LQFDSwapped != 0) {
            if (token0 == LQFD) {
                IUniswapV2Pair(LP).swap(
                    locals.LQFDReceived,
                    locals.tokenReceived,
                    address(this),
                    new bytes(0)
                );
            } else {
                IUniswapV2Pair(LP).swap(
                    locals.tokenReceived,
                    locals.LQFDReceived,
                    address(this),
                    new bytes(0)
                );
            }
        }

        IERC20(LQFD).safeTransfer(
            LP,
            LQFDAmount - locals.LQFDSwapped + locals.LQFDReceived
        );
        IERC20(token).safeTransfer(
            LP,
            tokenAmount - locals.tokenSwapped + locals.LQFDReceived
        );

        IUniswapV2Pair(LP).mint(mintTo);
    }
}
