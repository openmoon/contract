// SPDX-License-Identifier: AGPL-3.0-or-later

import "@openzeppelin/contracts/token/ERC20/IERC20.sol";

pragma solidity >=0.8.0 <=0.8.4;

interface IWETH is IERC20 {
    function deposit() external payable;

    function withdraw(uint256) external;
}
