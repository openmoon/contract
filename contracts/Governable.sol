// SPDX-License-Identifier: MIT

pragma solidity ^0.8.0;

abstract contract Governable {
    address public governor;

    modifier onlyGovernance() {
        require(governor == msg.sender, "LiquifiedGovernor: access denied");
        _;
    }

    function setGovernance(address governance) public onlyGovernance {
        governor = governance;
    }
}
