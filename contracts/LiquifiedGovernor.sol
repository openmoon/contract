// SPDX-License-Identifier: MIT
pragma solidity ^0.8.2;

import "@openzeppelin/contracts/utils/cryptography/ECDSA.sol";
import "@openzeppelin/contracts/utils/cryptography/draft-EIP712.sol";
import "@openzeppelin/contracts/utils/math/SafeCast.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "./Liquified.sol";
import "./Governable.sol";

contract LiquifiedGovernor is EIP712, Governable {
    enum ProposalState {
        Pending,
        Active,
        Defeated,
        Succeeded,
        Queued,
        Expired,
        Executed
    }

    event ProposalCreated(
        uint256 proposalId,
        address proposer,
        address[] targets,
        uint256[] values,
        string[] signatures,
        bytes[] calldatas,
        uint256 startTimestamp,
        uint256 endTimestamp,
        string description
    );

    enum VoteType {
        Against,
        For,
        Abstain
    }

    struct ProposalVote {
        uint256 againstVotes;
        uint256 forVotes;
        uint256 abstainVotes;
        mapping(address => bool) hasVoted;
    }

    struct ProposalCore {
        uint64 voteStart;
        uint64 voteEnd;
        bool exists;
        bool executed;
    }

    mapping(uint256 => ProposalVote) private _proposalVotes;
    mapping(uint256 => ProposalCore) private _proposals;
    bytes32 public constant BALLOT_TYPEHASH =
        keccak256("LiquifiedBallot(uint256 proposalId,uint8 support)");
    LiquifiedToken public token;
    uint256 public quorumNumerator = 100;
    uint256 public constant quorumDenominator = 1000;
    uint64 public votingDelay = 1 days;
    uint64 public votingPeriod = 3 days;

    event ProposalExecuted(uint256 proposalId);

    event VoteCast(
        address indexed voter,
        uint256 proposalId,
        uint8 support,
        uint256 weight,
        string reason
    );

    event QuorumNumeratorUpdated(
        uint256 oldQuorumNumerator,
        uint256 newQuorumNumerator
    );

    constructor(LiquifiedToken _token) EIP712("LiquifiedGovernor", "1.0.0") {
        token = _token;
        governor = address(this);
    }

    function setQuorumNumerator(uint256 newQuorumNumerator)
        public
        onlyGovernance
    {
        require(
            newQuorumNumerator <= quorumDenominator,
            "LiquifiedGovernor: quorumNumerator over quorumDenominator"
        );

        uint256 oldQuorumNumerator = quorumNumerator;
        quorumNumerator = newQuorumNumerator;

        emit QuorumNumeratorUpdated(oldQuorumNumerator, newQuorumNumerator);
    }

    function setVotingDelay(uint64 _votingDelay) public onlyGovernance {
        votingDelay = _votingDelay;
    }

    function setVotingPeriod(uint64 _votingPeriod) public onlyGovernance {
        votingPeriod = _votingPeriod;
    }

    function hashProposal(
        address[] memory targets,
        uint256[] memory values,
        bytes[] memory calldatas,
        bytes32 descriptionHash
    ) public pure returns (uint256) {
        return
            uint256(
                keccak256(
                    abi.encode(targets, values, calldatas, descriptionHash)
                )
            );
    }

    function state(uint256 proposalId) public view returns (ProposalState) {
        ProposalCore memory proposal = _proposals[proposalId];

        if (proposal.executed) {
            return ProposalState.Executed;
        } else if (proposal.voteStart >= block.timestamp) {
            return ProposalState.Pending;
        } else if (proposal.voteEnd >= block.timestamp) {
            return ProposalState.Active;
        } else if (proposal.voteEnd < block.timestamp) {
            return
                _quorumReached(proposalId) && _voteSucceeded(proposalId)
                    ? ProposalState.Succeeded
                    : ProposalState.Defeated;
        } else {
            revert("LiquifiedGovernor: unknown proposal id");
        }
    }

    function proposalSnapshot(uint256 proposalId)
        public
        view
        returns (uint256)
    {
        return _proposals[proposalId].voteStart;
    }

    function proposalDeadline(uint256 proposalId)
        public
        view
        returns (uint256)
    {
        return _proposals[proposalId].voteEnd;
    }

    function propose(
        address[] memory targets,
        uint256[] memory values,
        bytes[] memory calldatas,
        string memory description
    ) public returns (uint256) {
        uint256 proposalId = hashProposal(
            targets,
            values,
            calldatas,
            keccak256(bytes(description))
        );

        require(
            targets.length == values.length,
            "LiquifiedGovernor: invalid proposal length"
        );
        require(
            targets.length == calldatas.length,
            "LiquifiedGovernor: invalid proposal length"
        );
        require(targets.length > 0, "LiquifiedGovernor: empty proposal");

        ProposalCore storage proposal = _proposals[proposalId];
        require(!proposal.exists, "LiquifiedGovernor: proposal already exists");

        uint64 snapshot = SafeCast.toUint64(block.timestamp) + votingDelay;
        uint64 deadline = snapshot + votingPeriod;

        proposal.voteStart = snapshot;
        proposal.voteEnd = deadline;
        proposal.exists = true;

        emit ProposalCreated(
            proposalId,
            msg.sender,
            targets,
            values,
            new string[](targets.length),
            calldatas,
            snapshot,
            deadline,
            description
        );

        return proposalId;
    }

    function execute(
        address[] memory targets,
        uint256[] memory values,
        bytes[] memory calldatas,
        bytes32 descriptionHash
    ) public payable returns (uint256) {
        uint256 proposalId = hashProposal(
            targets,
            values,
            calldatas,
            descriptionHash
        );

        ProposalState status = state(proposalId);
        require(
            status == ProposalState.Succeeded || status == ProposalState.Queued,
            "LiquifiedGovernor: proposal not successful"
        );
        _proposals[proposalId].executed = true;

        emit ProposalExecuted(proposalId);

        _execute(proposalId, targets, values, calldatas, descriptionHash);

        return proposalId;
    }

    function _execute(
        uint256, /* proposalId */
        address[] memory targets,
        uint256[] memory values,
        bytes[] memory calldatas,
        bytes32 /*descriptionHash*/
    ) internal {
        string
            memory errorMessage = "LiquifiedGovernor: call reverted without message";
        for (uint256 i = 0; i < targets.length; ++i) {
            (bool success, bytes memory returndata) = targets[i].call{
                value: values[i]
            }(calldatas[i]);
            Address.verifyCallResult(success, returndata, errorMessage);
        }
    }

    function castVote(uint256 proposalId, uint8 support)
        public
        returns (uint256)
    {
        address voter = msg.sender;
        return _castVote(proposalId, voter, support, "");
    }

    function castVoteWithReason(
        uint256 proposalId,
        uint8 support,
        string calldata reason
    ) public returns (uint256) {
        address voter = msg.sender;
        return _castVote(proposalId, voter, support, reason);
    }

    function castVoteBySig(
        uint256 proposalId,
        uint8 support,
        uint8 v,
        bytes32 r,
        bytes32 s
    ) public returns (uint256) {
        address voter = ECDSA.recover(
            _hashTypedDataV4(
                keccak256(abi.encode(BALLOT_TYPEHASH, proposalId, support))
            ),
            v,
            r,
            s
        );
        require(voter != address(0), "ECDSA: invalid signature");
        return _castVote(proposalId, voter, support, "");
    }

    function _castVote(
        uint256 proposalId,
        address account,
        uint8 support,
        string memory reason
    ) internal returns (uint256) {
        ProposalCore storage proposal = _proposals[proposalId];
        require(
            state(proposalId) == ProposalState.Active,
            "LiquifiedGovernor: vote not currently active"
        );

        uint256 weight = getVotes(account, proposal.voteStart);
        _countVote(proposalId, account, support, weight);

        emit VoteCast(account, proposalId, support, weight, reason);

        return weight;
    }

    function getVotes(address account, uint256 timestamp)
        public
        view
        returns (uint256)
    {
        return token.getPastVotes(account, timestamp);
    }

    function quorum(uint256 timestamp) public view returns (uint256) {
        return
            (token.getPastTotalVotes(timestamp) * quorumNumerator) /
            quorumDenominator;
    }

    function hasVoted(uint256 proposalId, address account)
        public
        view
        returns (bool)
    {
        return _proposalVotes[proposalId].hasVoted[account];
    }

    function proposalVotes(uint256 proposalId)
        public
        view
        returns (
            uint256 againstVotes,
            uint256 forVotes,
            uint256 abstainVotes
        )
    {
        ProposalVote storage proposalvote = _proposalVotes[proposalId];
        return (
            proposalvote.againstVotes,
            proposalvote.forVotes,
            proposalvote.abstainVotes
        );
    }

    function _quorumReached(uint256 proposalId) internal view returns (bool) {
        ProposalVote storage proposalvote = _proposalVotes[proposalId];

        return
            quorum(proposalSnapshot(proposalId)) <=
            proposalvote.forVotes + proposalvote.abstainVotes;
    }

    function _voteSucceeded(uint256 proposalId) internal view returns (bool) {
        ProposalVote storage proposalvote = _proposalVotes[proposalId];

        return proposalvote.forVotes > proposalvote.againstVotes;
    }

    function _countVote(
        uint256 proposalId,
        address account,
        uint8 support,
        uint256 weight
    ) internal {
        ProposalVote storage proposalvote = _proposalVotes[proposalId];

        require(
            !proposalvote.hasVoted[account],
            "LiquifiedGovernor: vote already cast"
        );
        proposalvote.hasVoted[account] = true;

        if (support == uint8(VoteType.Against)) {
            proposalvote.againstVotes += weight;
        } else if (support == uint8(VoteType.For)) {
            proposalvote.forVotes += weight;
        } else if (support == uint8(VoteType.Abstain)) {
            proposalvote.abstainVotes += weight;
        } else {
            revert("LiquifiedGovernor: invalid value for enum VoteType");
        }
    }
}
