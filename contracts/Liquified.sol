// SPDX-License-Identifier: AGPL-3.0-or-later

import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/access/Ownable.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/utils/math/SafeMath.sol";
import "@openzeppelin/contracts/utils/math/SafeCast.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "@uniswap/v2-core/contracts/interfaces/IUniswapV2Pair.sol";
import "./MathUtils.sol";
import "./LiquifiedStaking.sol";
import "./Governable.sol";

pragma solidity >=0.8.0 <=0.8.4;

contract LiquifiedToken is IERC20, ReentrancyGuard, Governable {
    using Address for address;

    struct LPRecord {
        address token0;
        address token1;
    }

    struct Checkpoint {
        uint64 fromTimestamp;
        uint192 votes;
    }

    event RegisterLP(address indexed pair);
    event UnregisterLP(address indexed pair, string reason);
    event EnqueueLiquidity(address indexed pair, uint256 indexed amount);
    event RewardsBeneficiaryChanged(address from, address to);

    mapping(address => uint256) private value;
    mapping(address => uint256) public pendingLiquidity;
    mapping(address => mapping(address => uint256)) private _allowances;
    mapping(address => LPRecord) private records;
    uint256 public tokensTotal = 0; //total amount of tokens of redistribution-ineligible accounts
    uint256 public rTokensTotal = 1; //total amount of tokens of redistribution-eligible accounts
    uint256 public rValueTotal = 1_000_000; //total amount of R-values of redistribution-eligible accounts

    string public constant symbol = "LQFD";
    string public constant name = "Liquified Token";
    uint8 public constant decimals = 18;
    uint8 private percentBurned = 1;
    uint8 private percentRedistributed = 1;
    uint8 private percentLiquidified = 3;
    uint8 private percentForRewards = 1;

    address public stakingContract;
    address public migrationContract;
    address public rewardsBeneficiary =
        0xFFfFfFffFFfffFFfFFfFFFFFffFFFffffFfFFFfF;

    mapping(address => address) public delegates;
    mapping(address => Checkpoint[]) private checkpoints;
    Checkpoint[] private _totalVotesCheckpoints;
    mapping(address => uint192) lastDelegatorVotes;

    event DelegateChanged(
        address indexed delegator,
        address indexed fromDelegate,
        address indexed toDelegate
    );

    event DelegateVotesChanged(
        address indexed delegate,
        uint256 previousBalance,
        uint256 newBalance
    );

    constructor (address staking){
        governor = tx.origin; //temporitaly assign to deployer
        stakingContract = staking;
        LiquifiedStaking(staking).initialize();
    }

    function initialize() external {
        require(migrationContract == address(0), "LQFD: Already initialized");
        migrationContract = msg.sender;
        value[migrationContract] = rValueTotal;
        touchAccount(migrationContract);
        touchTotal();
        emit Transfer(address(0), msg.sender, rTokensTotal);
    }

    function setRates(
        uint8 burned,
        uint8 redistributed,
        uint8 liquidified,
        uint8 forRewards
    ) public onlyGovernance {
        percentBurned = burned;
        percentRedistributed = redistributed;
        percentLiquidified = liquidified;
        percentForRewards = forRewards;
    }

    function totalSupply() public view override returns (uint256) {
        return tokensTotal + rTokensTotal;
    }

    function allowance(address owner, address spender)
        public
        view
        override
        returns (uint256)
    {
        return _allowances[owner][spender];
    }

    function approve(address spender, uint256 amount)
        public
        override
        nonReentrant
        returns (bool)
    {
        _approve(msg.sender, spender, amount);
        return true;
    }

    function transferFrom(
        address sender,
        address recipient,
        uint256 amount
    ) public override nonReentrant returns (bool) {
        _transfer(sender, recipient, amount);

        uint256 currentAllowance = _allowances[sender][msg.sender];
        require(
            currentAllowance >= amount,
            "LQFD: transfer amount exceeds allowance"
        );
        _approve(sender, msg.sender, currentAllowance - amount);

        return true;
    }

    function transfer(address recipient, uint256 amount)
        public
        override
        nonReentrant
        returns (bool)
    {
        _transfer(msg.sender, recipient, amount);
        return true;
    }

    function burn(uint256 amount) public nonReentrant {
        require(balanceOf(msg.sender) >= amount);
        subtractBalance(msg.sender, amount);
        emit Transfer(msg.sender, address(0), amount);

        touchTotal();
        touchAccount(msg.sender);
    }

    function mint(address account, uint256 amount) public {
        require(msg.sender == migrationContract, "LQFD: Access denied!");
        addBalance(account, amount);
        emit Transfer(address(0), account, amount);

        touchTotal();
        touchAccount(account);
    }

    function _approve(
        address owner,
        address spender,
        uint256 amount
    ) internal {
        require(owner != address(0), "LQFD: approve from the zero address");
        require(spender != address(0), "LQFD: approve to the zero address");
        _allowances[owner][spender] = amount;
        emit Approval(owner, spender, amount);
    }

    function balanceOf(address account) public view override returns (uint256) {
        if (isExcluded(account)) return value[account];
        return tokenFromR(value[account]);
    }

    function _transfer(
        address sender,
        address recipient,
        uint256 amount
    ) internal {
        require(sender != address(0), "LQFD: transfer from the zero address");
        require(recipient != address(0), "LQFD: transfer to the zero address");
        require(
            balanceOf(sender) >= amount,
            "LQFD: transfer amount exceeds balance"
        );
        subtractBalance(sender, amount);
        bool takeFee = (sender != stakingContract) &&
            (recipient != stakingContract) &&
            (sender != migrationContract) &&
            (recipient != migrationContract);
        if (takeFee) {
            uint256 amountBurned = (amount * percentBurned) / 100;
            uint256 amountRedistributed = (amount * percentRedistributed) / 100;
            uint256 amountForRewards = (amount * percentForRewards) / 100;
            addRewardsValue(amountForRewards);
            distributeValue(amountRedistributed);
            amount =
                amount -
                amountRedistributed -
                amountBurned -
                amountForRewards;

            if (isLP(recipient)) {
                //try to liquidify, unregister LP on error
                uint256 amountLiquidified = (amount * percentLiquidified) / 100;
                try this.provideLiquidity(recipient, amountLiquidified) {
                    amount = amount - amountLiquidified;
                    _updateLiquidity(recipient);
                } catch Error(string memory reason) {
                    unregisterLP(recipient);
                    emit UnregisterLP(recipient, reason);
                }
            } else if (isLP(sender)) {
                uint256 amountLiquidified = (amount * percentLiquidified) / 100;
                amount = amount - amountLiquidified;
                addBalance(address(this), amountLiquidified);
                pendingLiquidity[sender] += amountLiquidified;
                emit EnqueueLiquidity(sender, amountLiquidified);
            }
        }
        addBalance(recipient, amount);
        emit Transfer(sender, recipient, amount);

        touchTotal();
        touchAccount(sender);
        touchAccount(recipient);
        touchAccount(rewardsBeneficiary);
    }

    //Checks if account is NOT eligible for redistribution.
    function isExcluded(address account) public view returns (bool) {
        return isLP(account) || (account == address(this));
    }

    function rFromToken(uint256 amount) public view returns (uint256) {
        return (amount * rValueTotal) / rTokensTotal;
    }

    function tokenFromR(uint256 amount) public view returns (uint256) {
        return (amount * rTokensTotal) / rValueTotal;
    }

    function distributeValue(uint256 amount) internal {
        rTokensTotal = rTokensTotal + amount;
    }

    function addBalance(address account, uint256 amount) internal {
        if (isExcluded(account)) {
            tokensTotal += amount;
            value[account] += amount;
        } else {
            uint256 rValue = rFromToken(amount);
            rTokensTotal += amount;
            rValueTotal += rValue;
            value[account] += rValue;
        }
    }

    function subtractBalance(address account, uint256 amount) internal {
        if (isExcluded(account)) {
            tokensTotal -= amount;
            value[account] -= amount;
        } else {
            uint256 rValue = rFromToken(amount);
            rTokensTotal -= amount;
            rValueTotal -= rValue;
            value[account] -= rValue;
        }
    }

    function convertToExcluded(address account) internal {
        require(!isExcluded(account), "LQFD: Account already excluded.");
        uint256 rValue = value[account];
        uint256 tokenV = tokenFromR(rValue);
        rValueTotal -= rValue;
        rTokensTotal -= tokenV;

        tokensTotal += tokenV;
        value[account] = tokenV;
    }

    function convertFromExcluded(address account) internal {
        require(isExcluded(account), "LQFD: Account not excluded.");
        uint256 tokenV = value[account];
        uint256 rValue = rFromToken(tokenV);
        tokensTotal -= tokenV;

        rTokensTotal += tokenV;
        rValueTotal += rValue;
        value[account] = rValue;
    }

    function isLP(address account) internal view returns (bool) {
        LPRecord storage rec = records[account];
        return (rec.token0 != address(0) && rec.token1 != address(0));
    }

    function unregisterLP(address account) internal {
        convertFromExcluded(account);
        LPRecord storage rec = records[account];
        rec.token0 = address(0);
        rec.token1 = address(0);
    }

    function provideLiquidity(address LP, uint256 amount)
        external
    {
        require(msg.sender == address(this), "LQFD: Access forebidden.");
        LPRecord storage rec = records[LP];
        bool LQFDtoken0 = rec.token0 == address(this);
        (uint256 reserve0, uint256 reserve1, ) = IUniswapV2Pair(LP)
            .getReserves();
        IERC20 otherToken = IERC20(LQFDtoken0 ? rec.token1 : rec.token0);
        uint256 LQFDReserve = LQFDtoken0 ? reserve0 : reserve1;
        uint256 otherReserve = LQFDtoken0 ? reserve1 : reserve0;
        uint256 amountIn = MathUtils.sqrt(
            LQFDReserve * (LQFDReserve + amount)
        ) - LQFDReserve;
        amountIn = (amountIn * 1025) / 1000;
        uint256 amountOut = (otherReserve * amountIn) /
            (LQFDReserve + amountIn);
        amountOut = (amountOut * 975) / 1000;
        uint256 otherBalBefore = otherToken.balanceOf(stakingContract);
        addBalance(LP, amountIn);
        IUniswapV2Pair(LP).swap(
            LQFDtoken0 ? 0 : amountOut,
            LQFDtoken0 ? amountOut : 0,
            stakingContract,
            new bytes(0)
        );
        uint256 otherReceived = otherToken.balanceOf(stakingContract) -
            otherBalBefore;
        LiquifiedStaking(stakingContract).transferTokens(
            address(otherToken),
            LP,
            otherReceived
        );
        addBalance(LP, amount - amountIn);
        IUniswapV2Pair(LP).mint(stakingContract);
    }

    function registerLP(address pair) external nonReentrant {
        convertToExcluded(pair);
        IUniswapV2Pair LP = IUniswapV2Pair(pair);
        address token0 = LP.token0();
        address token1 = LP.token1();
        require(
            (token0 == address(this)) != (token1 == address(this)),
            "LQFD: Invalid LP"
        );
        require(
            token0 != address(0) && token1 != address(0),
            "LQFD: Invalid LP!"
        );
        LPRecord memory rec = LPRecord(token0, token1);
        records[pair] = rec;
        emit RegisterLP(pair);

        touchTotal();
        touchAccount(pair);
    }

    function updateLiquidity(address LP) public nonReentrant {
        _updateLiquidity(LP);
        touchTotal();
        touchAccount(LP);
    }

    function touchTotal() internal {
        _writeCheckpoint(_totalVotesCheckpoints, uint192(rValueTotal));
    }

    function touchAccount(address account) internal {
        uint192 delegatorVotes = getDelegatorVotes(account);
        uint192 lastVotes = lastDelegatorVotes[account];
        if (delegates[account] != address(0)) {
            if (delegatorVotes > lastVotes) {
                uint192 delta = delegatorVotes - lastVotes;
                _writeCheckpoint(checkpoints[delegates[account]], true, delta);
            } else if (delegatorVotes < lastVotes) {
                uint192 delta = lastVotes - delegatorVotes;
                _writeCheckpoint(checkpoints[delegates[account]], false, delta);
            }
        }
        lastDelegatorVotes[account] = delegatorVotes;
    }

    function _updateLiquidity(address LP) internal {
        require(isLP(LP), "LQFD: Invalid LP!");
        uint256 pending = pendingLiquidity[LP];
        if (pending > 0) {
            subtractBalance(address(this), pending);
            pendingLiquidity[LP] = 0;

            try this.provideLiquidity(LP, pending) {} catch Error(
                string memory reason
            ) {
                unregisterLP(LP);
                emit UnregisterLP(LP, reason);
            }
        }
    }

    function addRewardsValue(uint256 amount) internal {
        addBalance(rewardsBeneficiary, amount);
    }

    function changeRewardsBeneficiary(address beneficiary)
        public
        onlyGovernance
    {
        require(
            beneficiary != rewardsBeneficiary,
            "LQFD: Invalid beneficiary address!"
        );
        emit RewardsBeneficiaryChanged(rewardsBeneficiary, beneficiary);
        uint256 amount = balanceOf(rewardsBeneficiary);
        subtractBalance(rewardsBeneficiary, amount);
        rewardsBeneficiary = beneficiary;
        addBalance(rewardsBeneficiary, amount);
    }

    function numCheckpoints(address account) public view returns (uint64) {
        return SafeCast.toUint64(checkpoints[account].length);
    }

    function getVotes(address account) public view returns (uint256) {
        uint256 pos = checkpoints[account].length;
        return pos == 0 ? 0 : checkpoints[account][pos - 1].votes;
    }

    function getTotalVotes() public view returns (uint256) {
        uint256 pos = _totalVotesCheckpoints.length;
        return pos == 0 ? 0 : _totalVotesCheckpoints[pos - 1].votes;
    }

    function getPastVotes(address account, uint256 timestamp)
        public
        view
        returns (uint256)
    {
        require(
            timestamp < block.timestamp,
            "LiquifiedToken: block not yet mined"
        );
        return _checkpointsLookup(checkpoints[account], timestamp);
    }

    function getPastTotalVotes(uint256 timestamp)
        public
        view
        returns (uint256)
    {
        require(
            timestamp < block.timestamp,
            "LiquifiedToken: block not yet mined"
        );
        return _checkpointsLookup(_totalVotesCheckpoints, timestamp);
    }

    function _checkpointsLookup(Checkpoint[] storage ckpts, uint256 timestamp)
        private
        view
        returns (uint256)
    {
        uint256 high = ckpts.length;
        uint256 low = 0;
        while (low < high) {
            uint256 mid = (low + high) / 2;
            if (ckpts[mid].fromTimestamp > timestamp) {
                high = mid;
            } else {
                low = mid + 1;
            }
        }

        return high == 0 ? 0 : ckpts[high - 1].votes;
    }

    function delegate(address delegatee) public {
        return _delegate(msg.sender, delegatee);
    }

    function _delegate(address delegator, address delegatee) internal virtual {
        address currentDelegate = delegates[delegator];
        uint256 delegatorVotes = getDelegatorVotes(delegator);
        delegates[delegator] = delegatee;

        emit DelegateChanged(delegator, currentDelegate, delegatee);

        _moveVotingPower(currentDelegate, delegatee, delegatorVotes);
    }

    function _moveVotingPower(
        address src,
        address dst,
        uint256 amount
    ) private {
        if (src != dst && amount > 0) {
            if (src != address(0)) {
                (uint256 oldWeight, uint256 newWeight) = _writeCheckpoint(
                    checkpoints[src],
                    false,
                    amount
                );
                emit DelegateVotesChanged(src, oldWeight, newWeight);
            }
            if (dst != address(0)) {
                (uint256 oldWeight, uint256 newWeight) = _writeCheckpoint(
                    checkpoints[dst],
                    true,
                    amount
                );
                emit DelegateVotesChanged(dst, oldWeight, newWeight);
            }
        }
    }

    function _writeCheckpoint(
        Checkpoint[] storage ckpts,
        bool additive,
        uint256 delta
    ) private returns (uint256 oldWeight, uint256 newWeight) {
        uint256 pos = ckpts.length;
        oldWeight = pos == 0 ? 0 : ckpts[pos - 1].votes;
        newWeight = additive ? oldWeight + delta : oldWeight - delta;

        if (pos > 0 && ckpts[pos - 1].fromTimestamp == block.timestamp) {
            ckpts[pos - 1].votes = uint192(newWeight);
        } else {
            ckpts.push(
                Checkpoint({
                    fromTimestamp: SafeCast.toUint64(block.timestamp),
                    votes: uint192(newWeight)
                })
            );
        }
    }

    function _writeCheckpoint(Checkpoint[] storage ckpts, uint192 val)
        internal
    {
        uint256 pos = ckpts.length;
        if (pos > 0 && ckpts[pos - 1].fromTimestamp == block.timestamp) {
            ckpts[pos - 1].votes = val;
        } else if (pos > 0 && ckpts[pos - 1].votes == val) {
            return;
        } else {
            ckpts.push(
                Checkpoint({
                    fromTimestamp: SafeCast.toUint64(block.timestamp),
                    votes: val
                })
            );
        }
    }

    function getDelegatorVotes(address account)
        public
        view
        returns (uint192 votes)
    {
        if (!isExcluded(account)) {
            votes += uint192(value[account]);
        }
        votes += uint192(
            LiquifiedStaking(stakingContract).accountRStake(account)
        );
    }
}
