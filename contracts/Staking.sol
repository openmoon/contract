// SPDX-License-Identifier: AGPL-3.0-or-later

import "@openzeppelin/contracts/token/ERC20/utils/SafeERC20.sol";
import "@openzeppelin/contracts/token/ERC20/IERC20.sol";
import "@openzeppelin/contracts/token/ERC20/extensions/IERC20Metadata.sol";
import "@openzeppelin/contracts/utils/Address.sol";
import "@openzeppelin/contracts/utils/Context.sol";
import "@openzeppelin/contracts/security/ReentrancyGuard.sol";
import "./OpenMoonToken.sol";
import "./MathUtils.sol";

pragma solidity >=0.8.0 <=0.8.4;

contract OpenMoonStakingContract is ReentrancyGuard {
    using SafeERC20 for IERC20;
    address public token;
    uint256 public constant lambdaConst = 9.749742196821812753717929 * 10**31;
    OpenMoon internal OPMN;
    mapping(address => PoolData) public pools;

    struct PoolData {
        MathUtils.ufixed512 rewardPerR;
        uint256 lastUpdateTime;
        uint256 availableRewardAmount;
        uint256 rewardReserve;
        uint256 rValueStakedTotal;
        mapping(address => uint256) rValueStaked;
        mapping(address => MathUtils.ufixed512) startingRPR;
    }

    constructor() {
        token = msg.sender;
        OPMN = OpenMoon(token);
    }

    function transferTokens(
        address _token,
        address to,
        uint256 amount
    ) external nonReentrant {
        require(msg.sender == token, "OPMN: Access forebidden.");
        IERC20(_token).safeTransfer(to, amount);
    }

    function updatePool(address reward) public {
        require(reward != token, "illegal reward");
        PoolData storage pool = pools[reward];
        uint256 deltaTime = block.timestamp - pool.lastUpdateTime;
        uint256 rewardBal = IERC20(reward).balanceOf(address(this));
        uint256 rewardAdded = rewardBal - pool.rewardReserve;
        if (pool.rValueStakedTotal > 0) {
            uint256 newAvailableReward = computeAvailableReward(
                pool.availableRewardAmount,
                deltaTime
            );
            pool.rewardPerR = MathUtils.add_512_512_512(
                pool.rewardPerR,
                MathUtils.div_256_256_512(
                    (pool.availableRewardAmount - newAvailableReward),
                    pool.rValueStakedTotal
                )
            );
            pool.availableRewardAmount = newAvailableReward;
        }
        pool.availableRewardAmount += rewardAdded;
        pool.rewardReserve = rewardBal;
        pool.lastUpdateTime = block.timestamp;
    }

    function updatePoolAccount(address reward, address account) internal {
        PoolData storage pool = pools[reward];
        updatePool(reward);
        uint256 rewardAmount = MathUtils.mul_256_512_256(
            pool.rValueStaked[account],
            (
                MathUtils.sub_512_512_512(
                    pool.rewardPerR,
                    pool.startingRPR[account]
                )
            )
        );
        pool.rewardReserve -= rewardAmount;
        IERC20(reward).safeTransfer(account, rewardAmount);
        pool.startingRPR[account] = pool.rewardPerR;
    }

    function deposit(address reward, uint256 amount) external nonReentrant {
        updatePoolAccount(reward, msg.sender);
        PoolData storage pool = pools[reward];
        IERC20(OPMN).safeTransferFrom(msg.sender, address(this), amount);
        uint256 rValue = OpenMoon(OPMN).rFromToken(amount);
        pool.rValueStaked[msg.sender] += rValue;
        pool.rValueStakedTotal += rValue;
    }

    function withdraw(address reward, uint256 amount) external nonReentrant {
        updatePoolAccount(reward, msg.sender);
        PoolData storage pool = pools[reward];
        IERC20(OPMN).safeTransfer(msg.sender, amount);
        uint256 rValue = OpenMoon(OPMN).rFromToken(amount);
        pool.rValueStaked[msg.sender] -= rValue;
        pool.rValueStakedTotal -= rValue;
    }

    function accountStake(address reward, address account)
        external
        view
        returns (uint256)
    {
        return OpenMoon(OPMN).tokenFromR(pools[reward].rValueStaked[account]);
    }

    function computeAvailableReward(
        uint256 currentAvailReward,
        uint256 deltaTime
    ) internal pure returns (uint256 availableRewardAmount) {
        uint256 divisor = MathUtils.generalExp(deltaTime * lambdaConst, 127);
        uint256 precision = 2**127;
        while (divisor >= 2**128) {
            divisor /= 2;
            precision /= 2;
        }
        MathUtils.ufixed512 memory multiplier = MathUtils.div_256_256_512(
            precision,
            divisor
        );
        return MathUtils.mul_256_512_256(currentAvailReward, multiplier);
    }

    function poolUserInfo(address reward, address account)
        external
        view
        returns (
            string memory rewardName,
            string memory rewardSymbol,
            uint256 stakeAccount,
            uint256 stakeTotal,
            uint256 rewardAvailable,
            uint256 rewardPending,
            uint256 OPMNBalanceAccount,
            uint256 OPMNAllowance
        )
    {
        PoolData storage pool = pools[reward];
        rewardName = IERC20Metadata(reward).name();
        rewardSymbol = IERC20Metadata(reward).symbol();
        stakeAccount = OpenMoon(OPMN).tokenFromR(pool.rValueStaked[account]);
        stakeTotal = OpenMoon(OPMN).tokenFromR(pool.rValueStakedTotal);
        rewardAvailable = pool.availableRewardAmount;
        OPMNBalanceAccount = OpenMoon(OPMN).balanceOf(account);
        OPMNAllowance = OpenMoon(OPMN).allowance(account, address(this));

        if (pool.rValueStakedTotal > 0) {
            MathUtils.ufixed512 memory newRewardPerR = MathUtils
                .add_512_512_512(
                    pool.rewardPerR,
                    MathUtils.div_256_256_512(
                        (pool.availableRewardAmount -
                            computeAvailableReward(
                                pool.availableRewardAmount,
                                block.timestamp - pool.lastUpdateTime
                            )),
                        pool.rValueStakedTotal
                    )
                );
            MathUtils.ufixed512 memory adjustedRpR = MathUtils.sub_512_512_512(
                newRewardPerR,
                pool.startingRPR[account]
            );

            rewardPending = MathUtils.mul_256_512_256(
                pool.rValueStaked[account],
                adjustedRpR
            );
        } else {
            rewardPending = 0;
        }
    }
}
